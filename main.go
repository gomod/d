package d

import (
	"fmt"

	"gitlab.com/gomod/e"
)

func Say(l int) {
	fmt.Printf("%v%v\n", "                       "[:l*2], "D: 1.4.0")
	e.Say(l + 1)
}
